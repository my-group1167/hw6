package JPC16HW6.dbexample;

import JPC16HW6.dbexample.dao.*;
import JPC16HW6.dbexample.model.Book;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class UserBean {
    private final UserDAOBean userDaoBean;
    private final BookDAOBean bookDaoBean;

    private UserBean(UserDAOBean userDaoBean, BookDAOBean bookDaoBean) {
        this.userDaoBean = userDaoBean;
        this.bookDaoBean = bookDaoBean;
    }

    public List<Book> findListBookUserByPhone(String str) throws SQLException {
        Integer id = userDaoBean.findIdUserByPhone(str);
        return methodBookList(id);
    }

    private List<Book> methodBookList(Integer id) throws SQLException {
        List<String> list = Arrays.stream
                        (Arrays.toString
                                        (userDaoBean
                                        .findUserById(id)
                                        .getListOfBookTitle())
                                .replaceAll("[\\[\\]{}\"]", "")
                                .split(","))
                .toList();
        List<Book> bookLists = new ArrayList<>();
        for (String s : list) {
            bookLists.add(bookDaoBean.findBookByTitle(s));
        }
        return bookLists;
    }

}
