package HW6.AddTask;

import java.util.Scanner;
import java.util.stream.LongStream;

public class AddTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число для проверки: ");
        long arm = scanner.nextLong();
        checkArmstrong(arm);
        simpleNumber(arm);
    }

    public static void checkArmstrong(Long x) {
        long xLength = String.valueOf(x).length();
        long xCheck = LongStream.iterate(x, i -> i / 10).limit(xLength).map(i -> (long) Math.pow(i % 10, xLength)).sum();
        System.out.println("Число " + x + " является числом Армстронга? - " + (x == xCheck));
    }

    public static void simpleNumber(Long x) {
        boolean flag = true;
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                flag = false;
                break;
            }
        }
        System.out.println("Число " + x + " является простым числом? - " + flag);
    }
}
