package JPC16HW6.dbexample.dao;

import JPC16HW6.dbexample.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Scope(scopeName = "prototype")
public class UserDAOBean {
    private final Connection connection;
    private final String ADD_USER = "insert into public.users(surname, name, birthday, phone, mail, listOfBookTitle) values (?, ?, ?, ?, ?, ?)";
    private final String SELECT_ID_USER_BY_PHONE_QUERY = "select public.users.id from public.users where phone = ?";

    public UserDAOBean(Connection connection) {
        this.connection = connection;
    }

    public void addUser(String surname, String name, String birthday, String phone, String mail, String[] listOfBookTitle) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(ADD_USER);
        statement.setString(1, surname);
        statement.setString(2, name);
        statement.setString(3, birthday);
        statement.setString(4, phone);
        statement.setString(5, mail);
        statement.setArray(6, statement.getConnection().createArrayOf("text", listOfBookTitle));
        statement.executeUpdate();
    }

    public User findUserById(Integer userId) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_ID_USER_BY_PHONE_QUERY);
        statement.setInt(1, userId);
        ResultSet resultSet = statement.executeQuery();
        User user = new User();
        while (resultSet.next()) {
            user.setSurname(resultSet.getString("surname"));
            user.setName(resultSet.getString("name"));
            user.setBirthday(resultSet.getDate("birthday"));
            user.setPhone(resultSet.getString("phone"));
            user.setMail(resultSet.getString("mail"));
            user.setListOfBookTitle(new String[]{resultSet.getString("listOfBookTitle")});
        }
        return user;
    }

    public Integer findIdUserByPhone(String phone) throws SQLException {
        return getIdUser(phone, SELECT_ID_USER_BY_PHONE_QUERY);
    }

    private Integer getIdUser(String phone, String selectIdUserByPhoneQuery) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(selectIdUserByPhoneQuery);
        statement.setString(1, phone);
        ResultSet resultSet = statement.executeQuery();
        int id = 0;
        while (resultSet.next()) {
            id = resultSet.getInt("id");
        }
        return id;
    }

}
