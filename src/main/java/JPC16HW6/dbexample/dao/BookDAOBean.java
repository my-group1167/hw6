package JPC16HW6.dbexample.dao;

import JPC16HW6.dbexample.model.Book;
import org.springframework.stereotype.Component;
import java.sql.*;

@Component
public class BookDAOBean {
    private final Connection connection;

    private final String SELECT_BOOK_BY_ID_QUERY = "select * from public.books where id = ?";
    private final String SELECT_BOOK_BY_TITLE_QUERY = "select * from public.books where title = ?";

    public BookDAOBean(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(Integer bookId) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(SELECT_BOOK_BY_ID_QUERY);
        selectQuery.setInt(1, bookId);
        ResultSet resultSet = selectQuery.executeQuery();
        return findBook(resultSet);
    }

    public Book findBookByTitle(String bookTitle) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(SELECT_BOOK_BY_TITLE_QUERY);
        selectQuery.setString(1, bookTitle);
        ResultSet resultSet = selectQuery.executeQuery();
        return findBook(resultSet);
    }

    private Book findBook(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        while (resultSet.next()) {
            book.setId(resultSet.getInt("id"));
            book.setAuthor(resultSet.getString("author"));
            book.setTitle(resultSet.getString("title"));
            book.setDateAdded(resultSet.getDate("date_added"));
        }
        return book;
    }
}