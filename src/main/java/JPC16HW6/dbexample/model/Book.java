package JPC16HW6.dbexample.model;

import lombok.*;

import java.util.Date;

// POJO - Plain Old Java Object
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
//@Data
public class Book {
    private Integer id;
    private String title;
    private String author;
    private Date dateAdded;
}



