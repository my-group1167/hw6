package JPC16HW6;

import JPC16HW6.dbexample.UserBean;
import JPC16HW6.dbexample.dao.BookDAOBean;
import JPC16HW6.dbexample.dao.UserDAOBean;
import JPC16HW6.dbexample.model.Book;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class SpringLibraryProjectApplication implements CommandLineRunner {
    private final BookDAOBean bookDaoBean;
    private final UserDAOBean userDaoBean;
    private final UserBean userBean;

    public SpringLibraryProjectApplication(BookDAOBean bookDaoBean, UserDAOBean userDaoBean, UserBean userBean) {
        this.bookDaoBean = bookDaoBean;
        this.userDaoBean = userDaoBean;
        this.userBean = userBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringLibraryProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        // Найти книгу по ID
//        System.out.println(bookDaoBean.findBookById(1));
//
//        // Добавить Пользователя
//        userDaoBean.addUser("Biketov", "Alexander", "1995-06-23", "+7(123)456-78-90",
//                "bikett@gmail.com", new String[]{"Путешествие из Петербурга в Москву", "Доктор Живаго", "Мёртвые души"});
//
//        // Получить ID Пользователя по телефону
//        int id = userDaoBean.findIdUserByPhone("+7(123)456-78-90");
//        System.out.println("ID ользователя с номером телефона \"+7(123)456-78-90\" = " + id);
//
        // Получить описание книг по телефону Пользователя
        List<Book> bookList2 = userBean.findListBookUserByPhone("+7(123)456-78-90");
        bookList2.forEach(System.out::println);
    }
}
