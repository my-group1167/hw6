package JPC16HW6.dbexample.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
    private Integer id;
    private String surname;
    private String name;
    private Date birthday;
    private String phone;
    private String mail;
    private String[] listOfBookTitle;
}
